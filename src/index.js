import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {BrowserRouter, Route, Switch} from 'react-router-dom'

import ReduxPromise from 'redux-promise'

import reducers from './reducers';

import MainNav from './components/main_nav';
import PostsIndex from './containers/post_index';
import PostNew from './containers/post_new';
import PostsShow from './components/posts_show';

const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);


ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <BrowserRouter>
            <div className='main-container'>
                    <MainNav/>
                <div className='row'>
                    <Route path="/" component={PostsIndex}/>
                    <Switch>
                        <Route path="/new" component={PostNew}/>
                        <Route path="/show/:id" component={PostsShow}/>
                    </Switch>
                </div>
            </div>
        </BrowserRouter>
    </Provider>
    , document.querySelector('.container'));
