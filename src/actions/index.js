import axios from 'axios';

const ROOT_URL = 'http://reduxblog.herokuapp.com/api/';
const API_KEY = '?key=markky21'
export const FETCH_POSTS = 'FETCH_POSTS';
export const FETCH_POST = 'FETCH_POST';
export const CREATE_POST = 'CREATE_POST'
export const DELETE_POST = 'DELETE_POST'

export function fetchPosts() {

    const request = axios.get(`${ROOT_URL}posts${API_KEY}`);

    return {
        type: FETCH_POSTS,
        payload: request
    }
}

export function createPost(postData, callback) {

    const request = axios.post(`${ROOT_URL}posts${API_KEY}`, postData)
        .then(() => {
            callback();
        });

    return {
        type: CREATE_POST,
        payload: request
    }
}

export function fetchPost(postId) {

    const request = axios.get(`${ROOT_URL}posts/${postId}${API_KEY}`);

    return {
        type: FETCH_POST,
        payload: request
    }

}

export function deletePost(postId, callback) {

    const request = axios.delete(`${ROOT_URL}posts/${postId}${API_KEY}`)
        .then(() => {
            callback();
        });

    return {
        type: DELETE_POST,
        payload: request
    }
}