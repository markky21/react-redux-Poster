import {FETCH_POSTS, FETCH_POST, DELETE_POST} from '../actions/index';


export default function FetchPostsReducer(state = {}, action) {

    // console.log(action.type);
    let posts = {};

    switch (action.type) {

        case DELETE_POST:
            posts = {...state};
            if (action.payload) {
                delete posts[action.payload.data.id]
            }
            return posts;
            break;

        case FETCH_POST:
            return {...state, [action.payload.data.id]: action.payload.data};
            break;

        case FETCH_POSTS:
            action.payload.data.map((post) => {
                posts[post.id] = post;
            });
            return posts;
            break;
    }

    return state;
}