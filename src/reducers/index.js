import {combineReducers} from 'redux';
import FetchPostsReducer from './reducer_fetch_posts';
import CreatePostReducer from './reducer_create_post';
import GetPostReducer from './reducer_fetch_post';

import {reducer as formReducer} from 'redux-form';

const rootReducer = combineReducers({
    posts: FetchPostsReducer,
    newPost: CreatePostReducer,
    viewedPost: GetPostReducer,
    form: formReducer
});

export default rootReducer;
