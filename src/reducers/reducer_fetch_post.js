import {FETCH_POST} from '../actions/index';

export default function GetPostReducer(state = {}, action) {

    switch (action.type) {
        case FETCH_POST:
            console.log(action);
            return action.payload.data;
            break;
    }

    return state;

}