import React from 'react';
import {Link} from 'react-router-dom';

export default (props) => {

    return (
        <nav className='col-12'>
            <ul className="nav">
                <li className="nav-link">
                    <Link className="nav-link active" to="/">
                        <h1>Poster!</h1>
                    </Link>
                </li>
                <li className="nav-link">
                    <Link className="nav-link active" to="/new">
                        Add a post
                    </Link>
                </li>
            </ul>

        </nav>
    )

}