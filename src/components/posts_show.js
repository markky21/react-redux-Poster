import React, {Component} from 'react';
import {connect} from 'react-redux';

import {fetchPost, deletePost} from '../actions/index';


class PostsShow extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const {id} = this.props.match.params;
        //this.props.fetchPost(id);
    }

    onDeleteClick() {
        const {id} = this.props.match.params;
        this.props.deletePost(id, () => {
            this.props.history.push('/');
        });
    }

    render() {
        const {post} = this.props;

        if (!post) {
            return (
                <section className='col-6 col-xs-12'>
                    Loading...
                </section>
            )
        }

        return (
            <section className='col-6 col-xs-12'>
                <h2>Show post</h2>
                <div className="card" style={{width: '100%'}}>
                    <div className="card-body">
                        <h3 className="card-title">{post.title}</h3>
                        <h6 className="card-title">{post.categories}</h6>
                        <p className="card-text">{post.content}</p>
                    </div>
                    <button className='btn btn-danger' onClick={this.onDeleteClick.bind(this)}>Delete the post</button>
                </div>
            </section>
        );
    }

}

function mapStateToProps({posts}, ownProps) {
    const {id} = ownProps.match.params;
    return {post: posts[id]};
}

export default connect(mapStateToProps, {fetchPost, deletePost})(PostsShow)