import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import {fetchPosts} from '../actions/index';

class PostsIndex extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.fetchPosts();
    }

    renderPost(post) {
        return (
            <li className='list-group-item list-group-item-action'
                key={post.id}>
                <Link to={`/show/${post.id}`}>
                    post: {post.title}
                </Link>
            </li>
        )
    }

    render() {
        return (
            <section className='col-12 col-md-6'>
                <h2>Posts</h2>
                <ul className='list-group'>
                    {
                        Object.keys(this.props.posts).map(
                            (key) => this.renderPost(this.props.posts[key])
                        )
                    }
                </ul>
            </section>
        );
    }

}

function mapStateToProps({posts}) {
    return {posts};
}

export default connect(mapStateToProps, {fetchPosts})(PostsIndex);

