import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {createPost} from '../actions/index';

import {Field, reduxForm} from 'redux-form';

const FIELDS = {
    title: {
        input: 'input',
        type: 'text',
        label: 'Post title:',
        errorMsg: 'Enter a title!',
        placeholder: 'New post title'
    },
    categories: {
        input: 'select',
        label: 'Select post categories:',
        errorMsg: 'Select a category!',
        options: ['', 'Hobby', 'Work', 'Music', 'News']
    },
    content: {
        input: 'textarea',
        label: 'Post content:',
        errorMsg: 'Enter of some content!',
        rows: 3
    }

};

class PostNew extends Component {

    renderField(fieldConfig) {

        const {
            label,
            type,
            placeholder,
            options,
            rows,
            meta: {touched, error}
        } = fieldConfig;

        return (
            <div className="form-group">
                <label>{fieldConfig.label}</label>
                <fieldConfig.inputType
                    className={'form-control' +
                    (touched && error ? ' is-invalid' : '') +
                    (touched && !error ? ' is-valid' : '')
                    }
                    type={type}
                    placeholder={placeholder}
                    rows={rows}
                    {...fieldConfig.input}
                >
                    {options ?
                        options.map((option) => <option key={'key_' + option}>{option}</option>) : null
                    }
                </fieldConfig.inputType>
                <div className="invalid-feedback">
                    {touched && error}
                </div>
            </div>
        )

    }

    onSubmit(values) {

        createPost(values, () => {
            this.props.history.push('/')
        });

    }

    render() {

        const {handleSubmit} = this.props;

        return (
            <section className='col-12 col-md-6'>
                <h2>Add new post</h2>
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>

                    {Object.keys(this.props.fields).map(key => {

                        const field = this.props.fields[key];

                        return (
                            <Field
                                key={key}
                                name={key}
                                inputType={field.input}
                                label={field.label}
                                type={field.type ? field.type : null}
                                placeholder={field.placeholder ? field.placeholder : null}
                                options={field.options ? field.options : null}
                                rows={field.rows ? field.rows : null}
                                component={this.renderField}
                            />
                        )
                    })}

                    <button type='submit' className='btn btn-primary mr-3'>Submit</button>
                    <Link to="/">
                        <button type='submit' className='btn btn-secondary'>Cancel</button>
                    </Link>
                </form>
            </section>
        );
    }

}

function validate(values) {

    const errors = {};

    Object.keys(FIELDS).map((key) => {
        if (!values[key]) {
            errors[key] = FIELDS[key].errorMsg
        }
    });

    return errors;
}

export default reduxForm({
    form: 'PostsNewForm',
    fields: FIELDS,
    validate,
})(
    connect(null, {createPost})(PostNew)
);


